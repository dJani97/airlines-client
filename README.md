# INNOlines client application

Cross-Platform client application for the INNOlines demo project

## Installing the framework
The Flutter framework requires separate installation. The steps can be found in this guide: https://flutter.dev/docs/get-started/install  

The requirements are different for Android, iOS and Web development. Both platforms require platform-specific tools and SDKs to be installed alongside Flutter. Platform-specific guides can also be found in the above documentation.  

Flutter version used for development:
```
Flutter 1.21.0-9.2.pre • channel beta • https://github.com/flutter/flutter.git
Framework • revision 81a45ec2e5 (10 days ago) • 2020-08-27 14:14:33 -0700
Engine • revision 20a9531835
Tools • Dart 2.10.0 (build 2.10.0-7.3.beta)
```

## Switching to Flutter Beta
At the time of writing, to compile this application for Web we must use Flutter Beta. The steps can be found here: https://flutter.dev/docs/get-started/web
The application can be compiled for android using the stable channel.

## Running the application

#### 1) Download the repository
`git clone git@bitbucket.org:dJani97/airlines-client.git`

#### 2) Get dependencies
`flutter pub get`

#### Run the app
`flutter run`

#### Build for Android
`flutter build apk`

#### Build for Web
`flutter build web`
