import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/models/web/built_city.dart';
import 'package:airlines_client/models/web/named_identifiable.dart';
import 'package:airlines_client/services/interfaces/Disposable.dart';
import 'package:rxdart/rxdart.dart';

class FlightPageModel implements Disposable {
  PublishSubject<NamedIdentifiable> any = PublishSubject<NamedIdentifiable>();

  BehaviorSubject<BuiltAirline> airline = BehaviorSubject<BuiltAirline>();
  BehaviorSubject<BuiltCity> from = BehaviorSubject<BuiltCity>();
  BehaviorSubject<BuiltCity> to = BehaviorSubject<BuiltCity>();

  FlightPageModel() {
    airline.stream.listen((event) => any.add(event));
    from.stream.listen((event) => any.add(event));
    to.stream.listen((event) => any.add(event));
  }

  isValid() => hasCities() && from.value.id != to.value.id;

  hasCities() => from.hasValue && to.hasValue;

  @override
  dispose() {
    airline.close();
    from.close();
    to.close();
    any.close();
  }
}
