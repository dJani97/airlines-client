import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'built_http_error.g.dart';

abstract class BuiltHttpError
    implements Built<BuiltHttpError, BuiltHttpErrorBuilder> {
  BuiltHttpError._();

  String get message;

  @nullable
  String get details;

  @nullable
  int get statusCode;

  factory BuiltHttpError([updates(BuiltHttpErrorBuilder b)]) = _$BuiltHttpError;

  static Serializer<BuiltHttpError> get serializer =>
      _$builtHttpErrorSerializer;
}
