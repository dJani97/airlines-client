// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'built_flight.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BuiltFlight> _$builtFlightSerializer = new _$BuiltFlightSerializer();

class _$BuiltFlightSerializer implements StructuredSerializer<BuiltFlight> {
  @override
  final Iterable<Type> types = const [BuiltFlight, _$BuiltFlight];
  @override
  final String wireName = 'BuiltFlight';

  @override
  Iterable<Object> serialize(Serializers serializers, BuiltFlight object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'source',
      serializers.serialize(object.source,
          specifiedType: const FullType(BuiltCity)),
      'destination',
      serializers.serialize(object.destination,
          specifiedType: const FullType(BuiltCity)),
      'airline',
      serializers.serialize(object.airline,
          specifiedType: const FullType(BuiltAirline)),
      'durationMinutes',
      serializers.serialize(object.durationMinutes,
          specifiedType: const FullType(int)),
      'distance',
      serializers.serialize(object.distance,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  BuiltFlight deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BuiltFlightBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'source':
          result.source.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltCity)) as BuiltCity);
          break;
        case 'destination':
          result.destination.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltCity)) as BuiltCity);
          break;
        case 'airline':
          result.airline.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltAirline)) as BuiltAirline);
          break;
        case 'durationMinutes':
          result.durationMinutes = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'distance':
          result.distance = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$BuiltFlight extends BuiltFlight {
  @override
  final String id;
  @override
  final BuiltCity source;
  @override
  final BuiltCity destination;
  @override
  final BuiltAirline airline;
  @override
  final int durationMinutes;
  @override
  final int distance;

  factory _$BuiltFlight([void Function(BuiltFlightBuilder) updates]) =>
      (new BuiltFlightBuilder()..update(updates)).build();

  _$BuiltFlight._(
      {this.id,
      this.source,
      this.destination,
      this.airline,
      this.durationMinutes,
      this.distance})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('BuiltFlight', 'id');
    }
    if (source == null) {
      throw new BuiltValueNullFieldError('BuiltFlight', 'source');
    }
    if (destination == null) {
      throw new BuiltValueNullFieldError('BuiltFlight', 'destination');
    }
    if (airline == null) {
      throw new BuiltValueNullFieldError('BuiltFlight', 'airline');
    }
    if (durationMinutes == null) {
      throw new BuiltValueNullFieldError('BuiltFlight', 'durationMinutes');
    }
    if (distance == null) {
      throw new BuiltValueNullFieldError('BuiltFlight', 'distance');
    }
  }

  @override
  BuiltFlight rebuild(void Function(BuiltFlightBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BuiltFlightBuilder toBuilder() => new BuiltFlightBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BuiltFlight &&
        id == other.id &&
        source == other.source &&
        destination == other.destination &&
        airline == other.airline &&
        durationMinutes == other.durationMinutes &&
        distance == other.distance;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), source.hashCode),
                    destination.hashCode),
                airline.hashCode),
            durationMinutes.hashCode),
        distance.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BuiltFlight')
          ..add('id', id)
          ..add('source', source)
          ..add('destination', destination)
          ..add('airline', airline)
          ..add('durationMinutes', durationMinutes)
          ..add('distance', distance))
        .toString();
  }
}

class BuiltFlightBuilder implements Builder<BuiltFlight, BuiltFlightBuilder> {
  _$BuiltFlight _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  BuiltCityBuilder _source;
  BuiltCityBuilder get source => _$this._source ??= new BuiltCityBuilder();
  set source(BuiltCityBuilder source) => _$this._source = source;

  BuiltCityBuilder _destination;
  BuiltCityBuilder get destination =>
      _$this._destination ??= new BuiltCityBuilder();
  set destination(BuiltCityBuilder destination) =>
      _$this._destination = destination;

  BuiltAirlineBuilder _airline;
  BuiltAirlineBuilder get airline =>
      _$this._airline ??= new BuiltAirlineBuilder();
  set airline(BuiltAirlineBuilder airline) => _$this._airline = airline;

  int _durationMinutes;
  int get durationMinutes => _$this._durationMinutes;
  set durationMinutes(int durationMinutes) =>
      _$this._durationMinutes = durationMinutes;

  int _distance;
  int get distance => _$this._distance;
  set distance(int distance) => _$this._distance = distance;

  BuiltFlightBuilder();

  BuiltFlightBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _source = _$v.source?.toBuilder();
      _destination = _$v.destination?.toBuilder();
      _airline = _$v.airline?.toBuilder();
      _durationMinutes = _$v.durationMinutes;
      _distance = _$v.distance;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BuiltFlight other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BuiltFlight;
  }

  @override
  void update(void Function(BuiltFlightBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BuiltFlight build() {
    _$BuiltFlight _$result;
    try {
      _$result = _$v ??
          new _$BuiltFlight._(
              id: id,
              source: source.build(),
              destination: destination.build(),
              airline: airline.build(),
              durationMinutes: durationMinutes,
              distance: distance);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'source';
        source.build();
        _$failedField = 'destination';
        destination.build();
        _$failedField = 'airline';
        airline.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BuiltFlight', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
