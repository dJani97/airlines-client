// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'built_city.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BuiltCity> _$builtCitySerializer = new _$BuiltCitySerializer();

class _$BuiltCitySerializer implements StructuredSerializer<BuiltCity> {
  @override
  final Iterable<Type> types = const [BuiltCity, _$BuiltCity];
  @override
  final String wireName = 'BuiltCity';

  @override
  Iterable<Object> serialize(Serializers serializers, BuiltCity object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  BuiltCity deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BuiltCityBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$BuiltCity extends BuiltCity {
  @override
  final String id;
  @override
  final String name;

  factory _$BuiltCity([void Function(BuiltCityBuilder) updates]) =>
      (new BuiltCityBuilder()..update(updates)).build();

  _$BuiltCity._({this.id, this.name}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('BuiltCity', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('BuiltCity', 'name');
    }
  }

  @override
  BuiltCity rebuild(void Function(BuiltCityBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BuiltCityBuilder toBuilder() => new BuiltCityBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BuiltCity && id == other.id && name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BuiltCity')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class BuiltCityBuilder implements Builder<BuiltCity, BuiltCityBuilder> {
  _$BuiltCity _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  BuiltCityBuilder();

  BuiltCityBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BuiltCity other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BuiltCity;
  }

  @override
  void update(void Function(BuiltCityBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BuiltCity build() {
    final _$result = _$v ?? new _$BuiltCity._(id: id, name: name);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
