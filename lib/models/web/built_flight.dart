import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/models/web/built_city.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'built_flight.g.dart';

abstract class BuiltFlight implements Built<BuiltFlight, BuiltFlightBuilder> {
  BuiltFlight._();

  String get id;

  BuiltCity get source;

  BuiltCity get destination;

  BuiltAirline get airline;

  @BuiltValueField(serialize: false)
  Duration get duration => Duration(minutes: durationMinutes);

  int get durationMinutes;

  int get distance;

  factory BuiltFlight([updates(BuiltFlightBuilder b)]) = _$BuiltFlight;

  static Serializer<BuiltFlight> get serializer => _$builtFlightSerializer;
}
