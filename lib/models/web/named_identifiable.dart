abstract class NamedIdentifiable {
  String get id;
  String get name;
}
