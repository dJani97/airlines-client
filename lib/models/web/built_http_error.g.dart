// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'built_http_error.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BuiltHttpError> _$builtHttpErrorSerializer =
    new _$BuiltHttpErrorSerializer();

class _$BuiltHttpErrorSerializer
    implements StructuredSerializer<BuiltHttpError> {
  @override
  final Iterable<Type> types = const [BuiltHttpError, _$BuiltHttpError];
  @override
  final String wireName = 'BuiltHttpError';

  @override
  Iterable<Object> serialize(Serializers serializers, BuiltHttpError object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.details != null) {
      result
        ..add('details')
        ..add(serializers.serialize(object.details,
            specifiedType: const FullType(String)));
    }
    if (object.statusCode != null) {
      result
        ..add('statusCode')
        ..add(serializers.serialize(object.statusCode,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  BuiltHttpError deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BuiltHttpErrorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'details':
          result.details = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'statusCode':
          result.statusCode = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$BuiltHttpError extends BuiltHttpError {
  @override
  final String message;
  @override
  final String details;
  @override
  final int statusCode;

  factory _$BuiltHttpError([void Function(BuiltHttpErrorBuilder) updates]) =>
      (new BuiltHttpErrorBuilder()..update(updates)).build();

  _$BuiltHttpError._({this.message, this.details, this.statusCode})
      : super._() {
    if (message == null) {
      throw new BuiltValueNullFieldError('BuiltHttpError', 'message');
    }
  }

  @override
  BuiltHttpError rebuild(void Function(BuiltHttpErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BuiltHttpErrorBuilder toBuilder() =>
      new BuiltHttpErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BuiltHttpError &&
        message == other.message &&
        details == other.details &&
        statusCode == other.statusCode;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, message.hashCode), details.hashCode), statusCode.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BuiltHttpError')
          ..add('message', message)
          ..add('details', details)
          ..add('statusCode', statusCode))
        .toString();
  }
}

class BuiltHttpErrorBuilder
    implements Builder<BuiltHttpError, BuiltHttpErrorBuilder> {
  _$BuiltHttpError _$v;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  String _details;
  String get details => _$this._details;
  set details(String details) => _$this._details = details;

  int _statusCode;
  int get statusCode => _$this._statusCode;
  set statusCode(int statusCode) => _$this._statusCode = statusCode;

  BuiltHttpErrorBuilder();

  BuiltHttpErrorBuilder get _$this {
    if (_$v != null) {
      _message = _$v.message;
      _details = _$v.details;
      _statusCode = _$v.statusCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BuiltHttpError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BuiltHttpError;
  }

  @override
  void update(void Function(BuiltHttpErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BuiltHttpError build() {
    final _$result = _$v ??
        new _$BuiltHttpError._(
            message: message, details: details, statusCode: statusCode);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
