import 'package:airlines_client/models/web/named_identifiable.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'built_city.g.dart';

abstract class BuiltCity
    implements Built<BuiltCity, BuiltCityBuilder>, NamedIdentifiable {
  BuiltCity._();

  String get id;

  String get name;

  factory BuiltCity([updates(BuiltCityBuilder b)]) = _$BuiltCity;

  static Serializer<BuiltCity> get serializer => _$builtCitySerializer;
}
