import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/models/web/built_flight.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'built_flight_plan.g.dart';

abstract class BuiltFlightPlan
    implements Built<BuiltFlightPlan, BuiltFlightPlanBuilder> {
  BuiltFlightPlan._();

  BuiltList<BuiltFlight> get flights;

  @BuiltValueField(serialize: false)
  Duration get totalDuration => Duration(minutes: totalDurationMinutes);

  int get totalDurationMinutes;

  BuiltAirline exclusiveAirline() {
    var airlines = flights.map((flight) => flight.airline).toSet();
    return (airlines.length == 1) ? airlines.first : null;
  }

  factory BuiltFlightPlan([updates(BuiltFlightPlanBuilder b)]) =
      _$BuiltFlightPlan;

  static Serializer<BuiltFlightPlan> get serializer =>
      _$builtFlightPlanSerializer;
}
