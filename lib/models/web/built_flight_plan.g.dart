// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'built_flight_plan.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BuiltFlightPlan> _$builtFlightPlanSerializer =
    new _$BuiltFlightPlanSerializer();

class _$BuiltFlightPlanSerializer
    implements StructuredSerializer<BuiltFlightPlan> {
  @override
  final Iterable<Type> types = const [BuiltFlightPlan, _$BuiltFlightPlan];
  @override
  final String wireName = 'BuiltFlightPlan';

  @override
  Iterable<Object> serialize(Serializers serializers, BuiltFlightPlan object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'flights',
      serializers.serialize(object.flights,
          specifiedType:
              const FullType(BuiltList, const [const FullType(BuiltFlight)])),
      'totalDurationMinutes',
      serializers.serialize(object.totalDurationMinutes,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  BuiltFlightPlan deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BuiltFlightPlanBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'flights':
          result.flights.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(BuiltFlight)]))
              as BuiltList<Object>);
          break;
        case 'totalDurationMinutes':
          result.totalDurationMinutes = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$BuiltFlightPlan extends BuiltFlightPlan {
  @override
  final BuiltList<BuiltFlight> flights;
  @override
  final int totalDurationMinutes;

  factory _$BuiltFlightPlan([void Function(BuiltFlightPlanBuilder) updates]) =>
      (new BuiltFlightPlanBuilder()..update(updates)).build();

  _$BuiltFlightPlan._({this.flights, this.totalDurationMinutes}) : super._() {
    if (flights == null) {
      throw new BuiltValueNullFieldError('BuiltFlightPlan', 'flights');
    }
    if (totalDurationMinutes == null) {
      throw new BuiltValueNullFieldError(
          'BuiltFlightPlan', 'totalDurationMinutes');
    }
  }

  @override
  BuiltFlightPlan rebuild(void Function(BuiltFlightPlanBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BuiltFlightPlanBuilder toBuilder() =>
      new BuiltFlightPlanBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BuiltFlightPlan &&
        flights == other.flights &&
        totalDurationMinutes == other.totalDurationMinutes;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, flights.hashCode), totalDurationMinutes.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BuiltFlightPlan')
          ..add('flights', flights)
          ..add('totalDurationMinutes', totalDurationMinutes))
        .toString();
  }
}

class BuiltFlightPlanBuilder
    implements Builder<BuiltFlightPlan, BuiltFlightPlanBuilder> {
  _$BuiltFlightPlan _$v;

  ListBuilder<BuiltFlight> _flights;
  ListBuilder<BuiltFlight> get flights =>
      _$this._flights ??= new ListBuilder<BuiltFlight>();
  set flights(ListBuilder<BuiltFlight> flights) => _$this._flights = flights;

  int _totalDurationMinutes;
  int get totalDurationMinutes => _$this._totalDurationMinutes;
  set totalDurationMinutes(int totalDurationMinutes) =>
      _$this._totalDurationMinutes = totalDurationMinutes;

  BuiltFlightPlanBuilder();

  BuiltFlightPlanBuilder get _$this {
    if (_$v != null) {
      _flights = _$v.flights?.toBuilder();
      _totalDurationMinutes = _$v.totalDurationMinutes;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BuiltFlightPlan other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BuiltFlightPlan;
  }

  @override
  void update(void Function(BuiltFlightPlanBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BuiltFlightPlan build() {
    _$BuiltFlightPlan _$result;
    try {
      _$result = _$v ??
          new _$BuiltFlightPlan._(
              flights: flights.build(),
              totalDurationMinutes: totalDurationMinutes);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'flights';
        flights.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BuiltFlightPlan', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
