import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'named_identifiable.dart';

part 'built_airline.g.dart';

abstract class BuiltAirline
    implements Built<BuiltAirline, BuiltAirlineBuilder>, NamedIdentifiable {
  BuiltAirline._();

  String get id;

  String get name;

  factory BuiltAirline([updates(BuiltAirlineBuilder b)]) = _$BuiltAirline;

  static Serializer<BuiltAirline> get serializer => _$builtAirlineSerializer;
}
