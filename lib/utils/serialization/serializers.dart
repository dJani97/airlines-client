library serializers;

import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/models/web/built_city.dart';
import 'package:airlines_client/models/web/built_flight.dart';
import 'package:airlines_client/models/web/built_flight_plan.dart';
import 'package:airlines_client/models/web/built_http_error.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  BuiltCity,
  BuiltAirline,
  BuiltFlight,
  BuiltFlightPlan,
  BuiltHttpError,
])
Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();
