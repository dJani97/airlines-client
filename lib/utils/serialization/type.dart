class _TypeHelper<T> {}

bool isSubtypeOf<SubT, T>() => _TypeHelper<SubT>() is _TypeHelper<T>;
