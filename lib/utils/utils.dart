import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

isDesktop(SizingInformation info) =>
    info.deviceScreenType == DeviceScreenType.desktop;
isMobile(SizingInformation info) =>
    info.deviceScreenType == DeviceScreenType.mobile;
isTablet(SizingInformation info) =>
    info.deviceScreenType == DeviceScreenType.tablet;

const noWidget = SizedBox(
  height: 0,
  width: 0,
);

String formatDuration(Duration duration) {
  if (duration == null) return '';
  String twoDigits(int n) => n.toString().padLeft(2, "0");
  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
}
