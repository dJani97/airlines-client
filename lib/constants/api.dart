const Duration kClientTimeout = const Duration(seconds: 3);

const protocol = 'http';
const port = '18080';
const kApiBaseUrl = '$protocol://80.211.202.225:$port';
const kApiCityPath = '/city';
const kApiFlightPath = '/flight';
const kApiAirlinePath = '/airline';
const kApiFlightPlanPath = '/flight-plan';
