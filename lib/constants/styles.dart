import 'package:airlines_client/constants/sizes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

//App
final ThemeData kThemeData = ThemeData(
  primarySwatch: kAppAccentColor,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);

// Text
const TextStyle kPageTitleStyle = TextStyle(fontSize: kPageTitleFontSize);
const TextStyle kListTitleStyle =
    TextStyle(fontWeight: FontWeight.w300, fontSize: 18, color: Colors.black54);
final TextStyle kListTitleLargeStyle = kListTitleStyle.copyWith(fontSize: 22);
const TextStyle kListHeaderStyle =
    TextStyle(fontWeight: FontWeight.w600, fontSize: 25);
const TextStyle kListSubHeaderStyle =
    TextStyle(fontWeight: FontWeight.w300, fontSize: 16, color: Colors.grey);
