const String kAppName = 'INNOlines';
const String kHomePage = 'Home';
const String kFlightsPage = 'Flights';

const String kBestRoute = 'Flight Plan Details';
const String kFlightsTitle = 'Flights';

const String kAirlinesTitle = 'Airlines';

const String kMenuAirline = 'Airline';
const String kMenuFrom = 'From';
const String kMenuTo = 'To';
const String kMenuHint = 'Type city name below...';

const String kError =
    'Please check your Internet connection and restart the app';
const String kNoFlightsFound = 'No flights for the selected route:(';
