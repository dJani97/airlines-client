import 'package:flutter/material.dart';

// App
const double kAppBasePadding = 16.0;
const double kAppSmallPadding = 8.0;
const double kAppMediumPadding = 12.0;
const double kListTitlePadding = kAppMediumPadding;

// NavBar
const double kLogoSize = 40.0;
const double kMobileNavBarHeight = 60.0;
const double kWebNavBarHeight = 100.0;
const double kNavDrawerWidth = 200.0;
const double kNavDrawerBlurRadius = 16.0;
const double kNavDrawerItemPadding = 60.0;
const double kWebSpaceBetweenNavItems = 100.0;

// Page
const double kCenteredPaddingHorizontal = kAppBasePadding;
const double kCenteredPaddingVertical = kAppSmallPadding;
const EdgeInsets kFlightsListPadding =
    const EdgeInsets.only(right: kAppBasePadding, left: kAppBasePadding);
const double kSpaceBetweenLists = 40.0;
const double kFlightsListHeightMobile = 500.0;
const double kFlightsListHeightDesktop = 800.0;

// Cards
const double kCardBaseElevation = 2.0;
const EdgeInsets kCardPadding = EdgeInsets.all(kAppBasePadding);

// Text
const double kPageTitleFontSize = 20.0;
const double kListTitleFontSize = 18.0;

// Menus
const double kMenuVerticalPadding = 10.0;
const double kMenusHeightMobile = 300.0;
const double kMenusHeightDesktop = 100.0;

// Lists
const EdgeInsets kListHeaderPadding = EdgeInsets.fromLTRB(
    kAppBasePadding, kAppBasePadding, kAppSmallPadding, kAppBasePadding);
