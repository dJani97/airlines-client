import 'package:airlines_client/locator.dart';
import 'package:airlines_client/routes/route_names.dart';
import 'package:airlines_client/routes/router.dart';
import 'package:airlines_client/services/navigation_service.dart';
import 'package:airlines_client/widgets/view/layout_template.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'constants/strings.dart';
import 'constants/styles.dart';

void main() {
  setUpLocator();

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: kAppName,
      theme: kThemeData,
      builder: (context, child) => NavigationTemplate(
        child: child,
      ),
      navigatorKey: locator<NavigationService>().navKey,
      onGenerateRoute: generateRoute,
      initialRoute: HomeRoute,
    );
  }
}
