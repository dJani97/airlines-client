import 'package:airlines_client/constants/anim.dart';
import 'package:airlines_client/constants/colors.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

class InteractiveText extends StatefulWidget {
  final String text;

  const InteractiveText({Key key, this.text}) : super(key: key);

  @override
  InteractiveTextState createState() => InteractiveTextState();
}

class InteractiveTextState extends State<InteractiveText> {
  static final appContainer =
      html.window.document.getElementById('app-container');

  final nonHoverTransform = Matrix4.identity()..translate(0, 0, 0);
  final hoverTransform = Matrix4.identity()..translate(0, 0, -6);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) {
        _isHovered(true);
        appContainer.style.cursor = 'pointer';
      },
      onExit: (_) {
        _isHovered(false);
        appContainer.style.cursor = 'default';
      },
      child: AnimatedContainer(
        duration: kAnimTextDuration,
        child: Text(
          widget.text,
          style: _hovering
              ? kPageTitleStyle.copyWith(
                  color: kAppAccentColor,
                  decoration: TextDecoration.underline,
                )
              : kPageTitleStyle,
        ),
        transform: _hovering ? hoverTransform : nonHoverTransform,
      ),
    );
  }

  _isHovered(bool hovered) {
    setState(() {
      _hovering = hovered;
    });
  }
}
