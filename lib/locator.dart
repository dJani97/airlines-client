import 'package:airlines_client/services/airline_service.dart';
import 'package:airlines_client/services/city_service.dart';
import 'package:airlines_client/services/flight_plan_service.dart';
import 'package:airlines_client/services/flight_service.dart';
import 'package:airlines_client/services/navigation_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => FlightPlanService());
  locator.registerLazySingleton(() => AirlineService());
  locator.registerLazySingleton(() => FlightService());
  locator.registerLazySingleton(() => CityService());
}
