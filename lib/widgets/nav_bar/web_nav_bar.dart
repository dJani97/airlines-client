import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/strings.dart';
import 'package:airlines_client/routes/route_names.dart';
import 'package:flutter/material.dart';

import '../logo.dart';
import 'nav_item.dart';

class NavigationBarWeb extends StatelessWidget {
  const NavigationBarWeb({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kWebNavBarHeight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          NavBarLogo(navPath: HomeRoute),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              NavBarItem(
                title: kHomePage,
                navPath: HomeRoute,
              ),
              SizedBox(width: kWebSpaceBetweenNavItems),
              NavBarItem(
                title: kFlightsPage,
                navPath: FlightsRoute,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
