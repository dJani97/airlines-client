import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:airlines_client/extensions/hover_extensions.dart';
import 'package:airlines_client/services/navigation_service.dart';
import 'package:airlines_client/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../locator.dart';

class NavBarItem extends StatefulWidget {
  final String title;
  final String navPath;

  const NavBarItem({@required this.title, @required this.navPath});

  @override
  _NavBarItemState createState() => _NavBarItemState();
}

class _NavBarItemState extends State<NavBarItem> {
  var navigatorService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (_, deviceInfo) => isMobile(deviceInfo)
          ? FlatButton(
              onPressed: () {
                navigatorService.navigateTo(widget.navPath);
                Scaffold.of(context).openDrawer();
              },
              child: Container(
                width: kNavDrawerWidth,
                child: Padding(
                  padding: const EdgeInsets.only(top: 30, bottom: 30),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      widget.title,
                      style: kPageTitleStyle,
                    ),
                  ),
                ),
              ),
            )
          : GestureDetector(
              onTap: () {
                navigatorService.navigateTo(widget.navPath);
              },
              child: InteractiveText(text: widget.title),
            ),
    );
  }
}
