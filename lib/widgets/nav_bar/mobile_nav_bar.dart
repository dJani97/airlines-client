import 'package:airlines_client/constants/sizes.dart';
import 'package:flutter/material.dart';

import '../logo.dart';

class NavigationBarMobile extends StatelessWidget {
  final VoidCallback onPressed;

  const NavigationBarMobile({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kMobileNavBarHeight,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          NavBarLogo(),
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: onPressed,
          ),
        ],
      ),
    );
  }
}
