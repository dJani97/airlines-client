import 'package:airlines_client/widgets/nav_bar/nav_item.dart';
import 'package:flutter/material.dart';

class NavDrawerItem extends StatelessWidget {
  final String title;
  final String navPath;

  const NavDrawerItem({this.title, this.navPath});

  @override
  Widget build(BuildContext context) {
    return NavBarItem(
      title: title,
      navPath: navPath,
    );
  }
}
