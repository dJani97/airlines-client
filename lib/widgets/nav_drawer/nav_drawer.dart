import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/strings.dart';
import 'package:airlines_client/routes/route_names.dart';
import 'package:flutter/material.dart';

import 'nav_drawer_item.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: kNavDrawerWidth,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: Colors.black12, blurRadius: kNavDrawerBlurRadius),
        ],
      ),
      child: Column(
        children: [
          NavDrawerItem(
            title: kHomePage,
            navPath: HomeRoute,
          ),
          NavDrawerItem(
            title: kFlightsPage,
            navPath: FlightsRoute,
          ),
        ],
      ),
    );
  }
}
