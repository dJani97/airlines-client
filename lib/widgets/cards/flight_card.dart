import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:airlines_client/models/web/built_flight.dart';
import 'package:flutter/material.dart';

class FlightInfoCard extends StatelessWidget {
  final BuiltFlight flight;

  const FlightInfoCard(this.flight);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: kCardBaseElevation,
      child: Padding(
        padding: kCardPadding,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              flight.airline.name,
              style: kListTitleStyle,
            ),
            Row(
              children: [
                Text(flight.source.name),
                Icon(Icons.arrow_right),
                Text(flight.destination.name),
              ],
            ),
            Text('${flight.distance} km'),
          ],
        ),
      ),
    );
  }
}
