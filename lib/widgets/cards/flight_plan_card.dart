import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:airlines_client/models/web/built_flight_plan.dart';
import 'package:airlines_client/utils/utils.dart';
import 'package:flutter/material.dart';

class FlightPlanInfoCard extends StatelessWidget {
  final BuiltFlightPlan plan;

  const FlightPlanInfoCard(this.plan);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: kCardBaseElevation,
      child: Padding(
        padding: kCardPadding,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Total flight time: ${formatDuration(plan?.totalDuration)}',
              style: kListTitleStyle,
            ),
            Text(
                '${plan.exclusiveAirline()?.name ?? 'Using multiple airlines'}'),
          ],
        ),
      ),
    );
  }
}
