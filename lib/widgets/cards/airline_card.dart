import 'package:airlines_client/constants/colors.dart';
import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:airlines_client/services/navigation_service.dart';
import 'package:airlines_client/utils/utils.dart' as utils;
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../locator.dart';

class AirlineCard extends StatelessWidget {
  final String navPath;
  final dynamic args;
  final String text;

  const AirlineCard({@required this.navPath, @required this.text, this.args});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, deviceInfo) {
        bool isMobile = utils.isMobile(deviceInfo);
        return Padding(
          padding: isMobile
              ? const EdgeInsets.only(
                  right: kAppMediumPadding,
                  left: kAppMediumPadding,
                  bottom: kAppSmallPadding)
              : const EdgeInsets.only(bottom: kAppSmallPadding),
          child: Card(
            color: Colors.white,
            elevation: kCardBaseElevation,
            child: ListTile(
              onTap: () {
                locator<NavigationService>()
                    .navigateTo(navPath, arguments: args);
              },
              leading: Icon(
                Icons.arrow_forward,
                color: kAppAccentColor,
              ),
              title: Text(
                text,
                style: kListTitleLargeStyle,
              ),
            ),
          ),
        );
      },
    );
  }
}
