import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/strings.dart';
import 'package:airlines_client/models/web/named_identifiable.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

class DropDownSearch<T extends NamedIdentifiable> extends StatefulWidget {
  final String label;
  final List<T> items;
  final String hint;
  final T selectedItem;
  final ValueChanged<T> onChanged;

  const DropDownSearch(
      {@required this.label,
      @required this.items,
      this.hint = kMenuHint,
      this.selectedItem,
      this.onChanged});

  @override
  _DropDownSearchState createState() => _DropDownSearchState();
}

class _DropDownSearchState<T extends NamedIdentifiable>
    extends State<DropDownSearch> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: kAppBasePadding, vertical: kMenuVerticalPadding),
      child: DropdownSearch<T>(
          validator: (v) => v == null ? "required field" : null,
          onChanged: widget.onChanged,
          mode: Mode.MENU,
          clearButton: Icon(Icons.close),
          showSelectedItem: true,
          showSearchBox: true,
          autoFocusSearchBox: true,
          items: widget.items,
          itemAsString: (item) => item.name,
          compareFn: (a, b) => a?.id == b?.id,
          label: widget.label,
          hint: widget.hint,
          selectedItem: widget.selectedItem ?? widget.items.first),
    );
  }
}
