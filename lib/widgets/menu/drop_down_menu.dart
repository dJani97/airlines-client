import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/models/web/named_identifiable.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

class DropDownMenu<T extends NamedIdentifiable> extends StatefulWidget {
  final String label;
  final List<T> items;
  final String hint;
  final T selectedItem;
  final ValueChanged<T> onChanged;

  const DropDownMenu(
      {@required this.label,
      @required this.items,
      this.hint,
      this.selectedItem,
      this.onChanged});

  @override
  _DropDownSearchState createState() => _DropDownSearchState();
}

class _DropDownSearchState<T extends NamedIdentifiable>
    extends State<DropDownMenu> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: kAppBasePadding, vertical: kMenuVerticalPadding),
      child: DropdownSearch<T>(
          onChanged: widget.onChanged,
          mode: Mode.MENU,
          showSelectedItem: true,
          showClearButton: true,
          items: widget.items,
          itemAsString: (item) => item.name,
          compareFn: (a, b) => a?.id == b?.id,
          label: widget.label,
          hint: widget.hint,
          selectedItem: widget.selectedItem),
    );
  }
}
