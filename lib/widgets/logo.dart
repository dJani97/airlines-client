import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/locator.dart';
import 'package:airlines_client/services/navigation_service.dart';
import 'package:flutter/material.dart';

class NavBarLogo extends StatefulWidget {
  final String navPath;

  const NavBarLogo({Key key, this.navPath}) : super(key: key);

  @override
  _NavBarLogoState createState() => _NavBarLogoState();
}

class _NavBarLogoState extends State<NavBarLogo> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        locator<NavigationService>().navigateTo(widget.navPath);
      },
      child: SizedBox(
        height: kLogoSize,
        width: kLogoSize,
        child: Image.asset('assets/logo.png'),
      ),
    );
  }
}
