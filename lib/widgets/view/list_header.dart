import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:airlines_client/utils/utils.dart';
import 'package:flutter/material.dart';

class ListHeader extends StatelessWidget {
  final String header;
  final String subHeader;

  const ListHeader({@required this.header, this.subHeader});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kListHeaderPadding,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            header,
            style: kListHeaderStyle,
          ),
          subHeader != null
              ? Text(
                  subHeader,
                  style: kListSubHeaderStyle,
                )
              : noWidget,
        ],
      ),
    );
  }
}
