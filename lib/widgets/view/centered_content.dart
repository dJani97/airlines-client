import 'package:airlines_client/constants/sizes.dart';
import 'package:flutter/material.dart';

class CenteredContent extends StatelessWidget {
  final Widget child;

  const CenteredContent({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: kCenteredPaddingHorizontal,
          vertical: kCenteredPaddingVertical),
      alignment: Alignment.topCenter,
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 1000),
        child: child,
      ),
    );
  }
}
