import 'package:airlines_client/utils/utils.dart';
import 'package:airlines_client/widgets/nav_bar/mobile_nav_bar.dart';
import 'package:airlines_client/widgets/nav_bar/web_nav_bar.dart';
import 'package:airlines_client/widgets/nav_drawer/nav_drawer.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'centered_content.dart';

class NavigationTemplate extends StatefulWidget {
  final Widget child;

  NavigationTemplate({Key key, this.child}) : super(key: key);

  @override
  _NavigationTemplateState createState() => _NavigationTemplateState();
}

class _NavigationTemplateState extends State<NavigationTemplate> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, deviceInfo) => SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          endDrawer: deviceInfo.deviceScreenType == DeviceScreenType.mobile
              ? NavDrawer()
              : null,
          backgroundColor: Colors.white,
          body: !isMobile(deviceInfo)
              ? CenteredContent(
                  child: Column(
                    children: [
                      NavigationBarWeb(),
                      Flexible(child: widget.child),
                    ],
                  ),
                )
              : Column(
                  children: [
                    CenteredContent(
                      child: NavigationBarMobile(
                        onPressed: () {
                          _scaffoldKey.currentState.openEndDrawer();
                        },
                      ),
                    ),
                    Flexible(child: widget.child),
                  ],
                ),
        ),
      ),
    );
  }
}
