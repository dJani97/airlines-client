import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/services/interfaces/Disposable.dart';
import 'package:airlines_client/services/web/airline_client.dart';
import 'package:built_collection/built_collection.dart';

class AirlineService implements Disposable {
  AirlineApiClient _client = AirlineApiClient.create();

  Future<BuiltList<BuiltAirline>> getAirlines() async =>
      (await _client.getAirlines()).body;

  dispose() => _client.dispose();
}
