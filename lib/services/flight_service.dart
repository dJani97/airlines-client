import 'package:airlines_client/models/web/built_flight.dart';
import 'package:airlines_client/services/interfaces/Disposable.dart';
import 'package:airlines_client/services/web/flight_client.dart';
import 'package:built_collection/built_collection.dart';

class FlightService implements Disposable {
  FlightApiClient _client = FlightApiClient.create();

  Future<BuiltList<BuiltFlight>> getCities({String airline}) async =>
      (await _client.getFlights(airline: airline)).body;

  dispose() => _client.dispose();
}
