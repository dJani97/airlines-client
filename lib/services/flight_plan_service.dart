import 'package:airlines_client/models/web/built_flight_plan.dart';
import 'package:airlines_client/services/interfaces/Disposable.dart';
import 'package:airlines_client/services/web/flight_plan_client.dart';

class FlightPlanService implements Disposable {
  FlightPlanApiClient _client = FlightPlanApiClient.create();

  Future<BuiltFlightPlan> getFlightPlan(String source, String destination,
          {String airline}) async =>
      airline != null
          ? (await _client.getFlightPlan(source, destination, airline: airline))
              .body
          : (await _client.getFlightPlan(source, destination)).body;

  dispose() => _client.dispose();
}
