import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
    return navKey.currentState.pushNamed(routeName, arguments: arguments);
  }

  void pop() {
    return navKey.currentState.pop();
  }
}
