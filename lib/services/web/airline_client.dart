import 'package:airlines_client/constants/api.dart';
import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/services/util/custom_request_interceptor.dart';
import 'package:airlines_client/utils/serialization/built_value_converter.dart';
import 'package:built_collection/built_collection.dart';
import 'package:chopper/chopper.dart';

part 'airline_client.chopper.dart';

@ChopperApi()
abstract class AirlineApiClient extends ChopperService {
  @Get(path: kApiAirlinePath)
  Future<Response<BuiltList<BuiltAirline>>> getAirlines();

  static AirlineApiClient create() {
    final client = ChopperClient(
      baseUrl: kApiBaseUrl,
      services: [_$AirlineApiClient()],
      interceptors: [
        HttpLoggingInterceptor(),
        CustomRequestInterceptor(),
      ],
      converter: BuiltValueConverter(),
    );

    return _$AirlineApiClient(client);
  }
}
