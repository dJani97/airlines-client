// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flight_plan_client.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$FlightPlanApiClient extends FlightPlanApiClient {
  _$FlightPlanApiClient([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = FlightPlanApiClient;

  @override
  Future<Response<BuiltFlightPlan>> getFlightPlan(
      String source, String destination,
      {String airline}) {
    final $url = '/flight-plan';
    final $params = <String, dynamic>{
      'source': source,
      'destination': destination,
      'airline': airline
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<BuiltFlightPlan, BuiltFlightPlan>($request);
  }
}
