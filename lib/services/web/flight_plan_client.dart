import 'package:airlines_client/constants/api.dart';
import 'package:airlines_client/models/web/built_flight_plan.dart';
import 'package:airlines_client/services/util/custom_request_interceptor.dart';
import 'package:airlines_client/utils/serialization/built_value_converter.dart';
import 'package:chopper/chopper.dart';

part 'flight_plan_client.chopper.dart';

@ChopperApi()
abstract class FlightPlanApiClient extends ChopperService {
  @Get(path: kApiFlightPlanPath)
  Future<Response<BuiltFlightPlan>> getFlightPlan(
      @Query() String source, @Query() String destination,
      {@Query() String airline});

  static FlightPlanApiClient create() {
    final client = ChopperClient(
      baseUrl: kApiBaseUrl,
      services: [_$FlightPlanApiClient()],
      interceptors: [
        HttpLoggingInterceptor(),
        CustomRequestInterceptor(),
      ],
      converter: BuiltValueConverter(),
    );

    return _$FlightPlanApiClient(client);
  }
}
