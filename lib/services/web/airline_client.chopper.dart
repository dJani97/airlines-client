// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'airline_client.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$AirlineApiClient extends AirlineApiClient {
  _$AirlineApiClient([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = AirlineApiClient;

  @override
  Future<Response<BuiltList<BuiltAirline>>> getAirlines() {
    final $url = '/airline';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<BuiltList<BuiltAirline>, BuiltAirline>($request);
  }
}
