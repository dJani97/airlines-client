// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flight_client.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$FlightApiClient extends FlightApiClient {
  _$FlightApiClient([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = FlightApiClient;

  @override
  Future<Response<BuiltList<BuiltFlight>>> getFlights({String airline}) {
    final $url = '/flight';
    final $params = <String, dynamic>{'airline': airline};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<BuiltList<BuiltFlight>, BuiltFlight>($request);
  }
}
