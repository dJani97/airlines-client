// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_client.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$CityApiClient extends CityApiClient {
  _$CityApiClient([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = CityApiClient;

  @override
  Future<Response<BuiltList<BuiltCity>>> getCities(
      {String id, String name, int population}) {
    final $url = '/city';
    final $params = <String, dynamic>{
      'id': id,
      'name': name,
      'population': population
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<BuiltList<BuiltCity>, BuiltCity>($request);
  }
}
