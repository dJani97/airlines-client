import 'package:airlines_client/constants/api.dart';
import 'package:airlines_client/models/web/built_flight.dart';
import 'package:airlines_client/services/util/custom_request_interceptor.dart';
import 'package:airlines_client/utils/serialization/built_value_converter.dart';
import 'package:built_collection/built_collection.dart';
import 'package:chopper/chopper.dart';

part 'flight_client.chopper.dart';

@ChopperApi()
abstract class FlightApiClient extends ChopperService {
  @Get(path: kApiFlightPath)
  Future<Response<BuiltList<BuiltFlight>>> getFlights(
      {@Query() String airline});

  static FlightApiClient create() {
    final client = ChopperClient(
      baseUrl: kApiBaseUrl,
      services: [_$FlightApiClient()],
      interceptors: [
        HttpLoggingInterceptor(),
        CustomRequestInterceptor(),
      ],
      converter: BuiltValueConverter(),
    );

    return _$FlightApiClient(client);
  }
}
