import 'package:airlines_client/constants/api.dart';
import 'package:airlines_client/models/web/built_city.dart';
import 'package:airlines_client/services/util/custom_request_interceptor.dart';
import 'package:airlines_client/utils/serialization/built_value_converter.dart';
import 'package:built_collection/built_collection.dart';
import 'package:chopper/chopper.dart';

part 'city_client.chopper.dart';

@ChopperApi()
abstract class CityApiClient extends ChopperService {
  @Get(path: kApiCityPath)
  Future<Response<BuiltList<BuiltCity>>> getCities(
      {@Query() String id, @Query() String name, @Query() int population});

  static CityApiClient create() {
    final client = ChopperClient(
      baseUrl: kApiBaseUrl,
      services: [_$CityApiClient()],
      interceptors: [
        HttpLoggingInterceptor(),
        CustomRequestInterceptor(),
      ],
      converter: BuiltValueConverter(),
    );

    return _$CityApiClient(client);
  }
}
