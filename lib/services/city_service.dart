import 'package:airlines_client/models/web/built_city.dart';
import 'package:airlines_client/services/interfaces/Disposable.dart';
import 'package:airlines_client/services/web/city_client.dart';
import 'package:built_collection/built_collection.dart';

class CityService implements Disposable {
  CityApiClient _client = CityApiClient.create();

  Future<BuiltList<BuiltCity>> getCities(
          {String id, String name, int population}) async =>
      (await _client.getCities(id: id, name: name, population: population))
          .body;

  dispose() => _client.dispose();
}
