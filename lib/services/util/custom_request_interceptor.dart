import 'dart:async';

import 'package:chopper/chopper.dart';

/// intercepts all requests
/// can be used for logging, debugging or modifying the content
class CustomRequestInterceptor
    implements RequestInterceptor, ResponseInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) {
    return request;
  }

  @override
  FutureOr<Response> onResponse(Response<dynamic> response) {
    return response;
  }
}
