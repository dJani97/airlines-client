import 'package:airlines_client/constants/sizes.dart';
import 'package:airlines_client/constants/strings.dart';
import 'package:airlines_client/constants/styles.dart';
import 'package:airlines_client/models/flight_page_model.dart';
import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/models/web/built_city.dart';
import 'package:airlines_client/models/web/built_flight.dart';
import 'package:airlines_client/models/web/built_flight_plan.dart';
import 'package:airlines_client/services/airline_service.dart';
import 'package:airlines_client/services/city_service.dart';
import 'package:airlines_client/services/flight_plan_service.dart';
import 'package:airlines_client/utils/utils.dart';
import 'package:airlines_client/widgets/cards/flight_card.dart';
import 'package:airlines_client/widgets/cards/flight_plan_card.dart';
import 'package:airlines_client/widgets/menu/drop_down_menu.dart';
import 'package:airlines_client/widgets/menu/drop_down_search.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:futuristic/futuristic.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../locator.dart';

class FlightsPage extends StatefulWidget {
  final BuiltAirline airline;

  FlightsPage({this.airline});

  @override
  _FlightsPageState createState() => _FlightsPageState();
}

class _FlightsPageState extends State<FlightsPage> {
  final _formKey = GlobalKey<FormState>();
  FlightPageModel model = FlightPageModel();

  @override
  void initState() {
    if (widget.airline != null) model.airline.add(widget.airline);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, deviceInfo) => CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Futuristic<BuiltList<BuiltAirline>>(
                autoStart: true,
                futureBuilder: () => locator<AirlineService>().getAirlines(),
                dataBuilder: (context, BuiltList<BuiltAirline> airlines) {
                  return Futuristic<BuiltList<BuiltCity>>(
                    autoStart: true,
                    futureBuilder: () => locator<CityService>().getCities(),
                    dataBuilder: (_, BuiltList<BuiltCity> cities) {
                      if (cities.length > 1 && !model.hasCities()) {
                        model.from.add(cities[0]);
                        model.to.add(cities[1]);
                      }
                      return Padding(
                        padding: const EdgeInsets.only(top: kAppBasePadding),
                        child: Form(
                          key: _formKey,
                          autovalidate: true,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: isDesktop(deviceInfo)
                                    ? kMenusHeightDesktop
                                    : kMenusHeightMobile,
                                child: !isDesktop(deviceInfo)
                                    ? Column(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children:
                                            _buildDropDowns(airlines, cities),
                                      )
                                    : Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children:
                                            _buildDropDowns(airlines, cities),
                                      ),
                              ),
                              Padding(
                                padding: kFlightsListPadding,
                                child: Column(
                                  children: [
                                    Text(
                                      kBestRoute,
                                      style: kListHeaderStyle,
                                    ),
                                    SizedBox(height: kAppBasePadding),
                                    StreamBuilder(
                                      stream: model.any,
                                      builder: (context, snapshot) =>
                                          model.isValid()
                                              ? _buildFlightPlan(deviceInfo)
                                              : Container(),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: kSpaceBetweenLists),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }),
          )
        ],
      ),
    );
  }

  List<Widget> _buildDropDowns(
      BuiltList<BuiltAirline> airlines, BuiltList<BuiltCity> cities) {
    return [
      Expanded(
          child: DropDownMenu(
        label: kMenuAirline,
        items: airlines.toList(),
        onChanged: (value) => model.airline.add(value),
        selectedItem: model.airline.value,
      )),
      Expanded(
          child: DropDownSearch(
        label: kMenuFrom,
        items: cities.toList(),
        onChanged: (value) => model.from.add(value),
        selectedItem: model.from.value,
      )),
      Expanded(
          child: DropDownSearch(
        label: kMenuTo,
        items: cities.toList(),
        onChanged: (value) => model.to.add(value),
        selectedItem: model.to.value,
      )),
    ];
  }

  Widget _buildFlightPlan(SizingInformation deviceInfo) {
    return FutureBuilder<BuiltFlightPlan>(
        future: locator<FlightPlanService>().getFlightPlan(
          model.from.value.id,
          model.to.value.id,
          airline: model.airline.value?.id,
        ),
        builder: (_, AsyncSnapshot<BuiltFlightPlan> plan) {
          if (plan.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          }
          if (plan.hasError) {
            Center(child: Text(kError));
          }
          if (plan.connectionState == ConnectionState.done) {
            print(plan);
            print(plan.data);
            return plan.data != null
                ? Container(
                    width: MediaQuery.of(context).size.width,
                    height: isDesktop(deviceInfo)
                        ? kFlightsListHeightDesktop
                        : kFlightsListHeightMobile,
                    child: Column(
                      children: [
                        FlightPlanInfoCard(plan.data),
                        SizedBox(height: kSpaceBetweenLists),
                        _buildList(plan.data?.flights)
                      ],
                    ),
                  )
                : Text(
                    kNoFlightsFound,
                    style: kListTitleStyle,
                  );
          }
          return CircularProgressIndicator();
        });
  }

  Widget _buildList(BuiltList<BuiltFlight> list) => Flexible(
        fit: FlexFit.tight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              kFlightsTitle,
              style: kListHeaderStyle,
            ),
            SizedBox(height: kAppBasePadding),
            Flexible(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return FlightInfoCard(list[index]);
                },
                itemCount: list.length,
              ),
            ),
          ],
        ),
      );
}
