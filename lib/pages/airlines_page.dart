import 'package:airlines_client/constants/strings.dart';
import 'package:airlines_client/locator.dart';
import 'package:airlines_client/models/web/built_airline.dart';
import 'package:airlines_client/routes/route_names.dart';
import 'package:airlines_client/services/airline_service.dart';
import 'package:airlines_client/services/navigation_service.dart';
import 'package:airlines_client/widgets/cards/airline_card.dart';
import 'package:airlines_client/widgets/view/list_header.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:futuristic/futuristic.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return Futuristic<BuiltList<BuiltAirline>>(
        autoStart: true,
        futureBuilder: locator<AirlineService>().getAirlines,
        dataBuilder: buildAirlineList,
        onError: (error, retry) => Center(
              child: Text(kError),
            ));
  }

  Widget buildAirlineList(_, BuiltList<BuiltAirline> data) {
    return ListView.builder(
      itemCount: data.length + 1,
      itemBuilder: (context, index) {
        if (index == 0) return ListHeader(header: kAirlinesTitle);
        return AirlineCard(
            navPath: FlightsRoute,
            args: data[index - 1],
            text: data[index - 1].name);
      },
    );
  }
}
