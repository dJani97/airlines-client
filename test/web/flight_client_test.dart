import 'package:airlines_client/services/web/flight_client.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('FlightApiClient', () async {
    var client = FlightApiClient.create();
    var items = (await client.getFlights()).body;
    for (var item in items) {
      print(item);
    }

    assert(items.isNotEmpty);
    assert(items[0].id != null);
    assert(items[0].airline.id != null);
    assert(items[0].source.id != null);
    assert(items[0].destination.id != null);
    assert(items[0].distance != null);
    assert(items[0].durationMinutes != null);

    client.dispose();
  });
}
