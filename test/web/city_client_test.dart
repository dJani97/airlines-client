import 'package:airlines_client/services/web/city_client.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('CityApiClient', () async {
    var client = CityApiClient.create();
    var items = (await client.getCities()).body;
    for (var item in items) {
      print(item);
    }

    assert(items.isNotEmpty);
    assert(items[0].id != null);
    assert(items[0].name != null);

    client.dispose();
  });
}
