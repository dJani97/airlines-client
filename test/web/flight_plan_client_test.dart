import 'package:airlines_client/services/web/city_client.dart';
import 'package:airlines_client/services/web/flight_plan_client.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('FlightPlanApiClient', () async {
    var cityClient = CityApiClient.create();
    var flightPlanClient = FlightPlanApiClient.create();
    var cities = (await cityClient.getCities()).body;

    assert(cities.length > 1);
    assert(cities[0].id != null);
    assert(cities[1].id != null);

    var flightPlan =
        (await flightPlanClient.getFlightPlan(cities[0].id, cities[1].id)).body;

    print(flightPlan);

    assert(flightPlan.flights != null);
    assert(flightPlan.flights.isNotEmpty);
    assert(flightPlan.totalDurationMinutes > 0);

    cityClient.dispose();
    flightPlanClient.dispose();
  });
}
