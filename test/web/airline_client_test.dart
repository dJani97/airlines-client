import 'package:airlines_client/services/web/airline_client.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('AirlineApiClient', () async {
    var client = AirlineApiClient.create();
    var items = (await client.getAirlines()).body;
    for (var item in items) {
      print(item);
    }

    assert(items.isNotEmpty);
    assert(items[0].id != null);
    assert(items[0].name != null);

    client.dispose();
  });
}
